# Evidence supporting my claims against Mario

### Note this is very incomplete, I have a lot more instances, will add when time allows, and also some PMs but have to ask for permission to share.

My claims:
- He knowingly and intentionally falsely accused me of distributing cheats in public several times
- He tried gaslighting me about it
- He has repetitively shown signs of other manipulative behavior over a long timeframe (confirmed by LH)
- He has selectively slowed down contributions by me (TODO examples)

Things i can't prove:
- He asked or incited musicgoat to insult me and spread lies about me on servers, then added him to credits as a reward (TODO example logs)

My impression is he has been trying to demotivate me from working on xon and is trying to get rid of me. Possible explanations:
- He considers me a more competent dev and feels his position is threatened (TODO examples where he clearly doesn't understand basic programming concepts)
- He wants to stop the port to rust (again, might feel threatened if he's incapable of learning the lang)
- He wants people to see xon as his game (consistent with LH's theory he's a covert narcissist). He will never say it outright, just behaves that way which reinforces it.
    - Some examples:
        - he keeps calling it his game - "thank you for a playing a my game a" TODO more examples
        - 2018-12-07 00:14:39    @Mario  if i just changed the default balance like that without looking at all the potential issues, i would be a dictator, and a bad one at that
            - Implying he has the power but is benevolent.
        - His friend, MusicGoat, talking about him as "head dev" showing he presents thatwas to him too


See the txt files:
    - [logs-freenode-xonotic.txt](logs-freenode-xonotic.txt)
    - [logs-qnet-xonotic.txt](logs-qnet-xonotic.txt)

- My comments are marked with `@@@`, no need to read all of it, mostly just the parts before my notes. The rest is there for context.
- The logs are from Solid's bouncer, he didn't want them to be made completely public though.
- I am not even sure they're complete, the timestamps are a bit weird sometimes (e.g. 2019-09-23 for 2 days) and i can't find a couple conversations i clearly recall.

Meanwhile, Mario is trying to kick me from the team for supposedly slandering him and the team. Moro sent mario a screenshot of said "slander" - see [Screenshot_20210308_010451.png](Screenshot_20210308_010451.png) - I stand behind everything i said there and i also don't see how this is in any way "unacceptable" as div said - everything there is true and this is not what i told others, i just asked questions.

I am ok with being kicked from the team later (for things i said 2+ years ago) but
- I do note nobody made a big deal out of them until it became convenient
- Mario has behaved in worse ways, though he was more subtle about it
- Mario's behavior is what caused me to talk to other people about it in the first place - it was not slander, I didn't even tell details to most people, just asked questions - now he is trying to spin it as slandering him and the team to "key figures in the community" and "a long line of community members" - in fact i talked to about 15 people, mostly who have quit xon and asked if they've seen manipulative behavior.

### Misc

Some of these are outright lying or insults, others are just a vague pattern of manipulation. Some could have plausible explanations

Not chronological.

(Use the invite to discord from [xonotic.org](xonotic.org) first so they work for you)

- He claimed I cheated in xon, intentionally forgetting to say I told everyone i was testing a triggerbot as a dev to make sure xon's cheat detection worked and that none of the players (one of who was a Core Team Member) objected. As a result, many people interpreted it as me trying to cheat to gain an advantage and gtting caught. These lies were then spread by his friend MusicGoat on servers where i didn't even have a chance to defend myself and explain the facts.
    - He refused to answer why he was twisting the truth this way - not disputing me, not apologizing, just pretending to be AFK, then coming back as if i said nothing. Repeatedly.

- Acusing my of **distrubuting** cheats yet another time - 2020-04-05
    - https://discord.com/channels/164477673584853001/164477673584853001/696289498501480469
        - First accusation
    - https://discord.com/channels/164477673584853001/164477673584853001/696376332770344980
        - My response
        - After this he proceeded to ignore all my questions regarding this for several days
    - https://gitlab.com/xonotic/xonotic-data.pk3dir/-/issues/2405#note_317842260
        - He clearly was online (as is seen from discord logs as well)
    - https://discord.com/channels/164477673584853001/164477673584853001/696447343246966834
        - Even Solid noticed he is avoiding the question to make me angry and wanted an answer.
        - This went on for several days, I asked again on QuakeNet and FreeNode, never got an answer

- Insulting my server.cfg MR, also ignoring questions
    - https://gitlab.com/xonotic/xonotic/-/merge_requests/58#note_314629410

- Calling me "the worst" (and we know xon has attracted some really bad people)
    - https://discord.com/channels/164477673584853001/164477673584853001/696392166804815974

- https://forums.xonotic.org/showthread.php?tid=8117&pid=85094#pid85094
    - Implying he is the one who decides if crylink gets rebalanced - note "most certainly".

- Selectively slowing down my MRs
    - Making me go through a team poll and forum poll for even small changes - e.g. flag autoreturn in CTF from 15 s to 30 s.
    - Meanwhile terence's MR which overhauled FT (major gameplay change) was merged without any poll at all.
        - https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/608
    - Making me create a separate testing balance for any change I propose, meanwhile other devs ran modified servers appearing to be pure for years.

- Breaks my MR for a second time (showing it's not accidental, especially since i asked for feedback on it earlier), then asking "What's the status of this"
    - https://gitlab.com/xonotic/xonotic-data.pk3dir/-/merge_requests/724

- He destroyed the Overkill mod by putting it near the bottom of the server list so it wouldn't get new players. He also introduced several bugs which made it unplayable for months (e.g. holding grenades meant all shots went exactly horizontally, no elevation). Each time me and LX updated, there was a new bug and we had to find some old revision which barely worked. The bugs could be explained as incompetence but he didn't hide his dislike for the mod.

- Blocking Dr. Jaska's suggestion about changes to electro
    - https://discord.com/channels/164477673584853001/164477673584853001/777698915457040395
    - All this while promoting his change to the balance...
        - 01:51 Mario : there's actually a branch in development that makes the electro combo explosion last longer than a single frame, for the sake of area control vs raw damage
    - ... and completely ignoring my own proposed alternative

- Insulting me: <Mario> well, they do say the highly intelligent are more likely to suffer mental illnesses.... i don't think anyone here thinks you're stupid, we just expect better from someone so capable
    - https://discord.com/channels/164477673584853001/164477673584853001/696416782302576691

- Putting a DO NOT ENTER icon on jeff's servers after their drama.

- Finally, making a "farewell" thread on the forums with more slight but significant tweaks to the truth:
    - https://forums.xonotic.org/showthread.php?tid=8522&pid=87112
    - Me pointing out lies: https://forums.xonotic.org/showthread.php?tid=8522&pid=87112#pid87112
